# CuraprofileToJSON

Simple CLI to convert curaprofiles to json or compare them.

## Installation

```sh
npm i -g curaprofile2json
```

## Usage

To simply output JSON as STDOUT
```sh
cura2json -f ./my-cura-profile.curaprofile
```

To output to file
```sh
cura2json -f ./my-cura-profile.curaprofile -o ./my-profile.json
```

To compare two profiles
```sh
cura2json -f ./my-cura-profile.curaprofile -c ./my-other-cura-profile.curaprofile
```

Also see
```sh
cura2json --help
```
