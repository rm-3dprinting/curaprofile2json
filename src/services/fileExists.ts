import fs from 'fs';

export default (path:string, exit=true):boolean => {
  try {
    fs.accessSync(path);
    return true;
  } catch (err) {
    if (exit){
      console.error(`${path} not found`);
      process.exit();
    }
    return false;
  }
};