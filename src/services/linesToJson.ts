export default (lines:string[]):Record<string,string|number> => {
  const result = {};
  for (const line of lines){
    const keyVal = line.split('=').map(a => a.trim());
    result[keyVal[0]] = !isNaN(parseInt(keyVal[1])) ? parseInt(keyVal[1]) : keyVal[1];
  }

  return result;
};