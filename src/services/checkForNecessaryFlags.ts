export default (flags:Record<string,string>, necessary = ['file']):void => {
  for (const key of necessary){
    if (!flags[key]){
      console.error(`Flag --${key} was not provided but is necessary`);
      process.exit(1);
    }
  }
};