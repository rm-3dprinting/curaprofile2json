export default (file:string):string[] =>
  file
    .replace(new RegExp('\'', 'g'), '')
    .split('\n')
    .filter(a => a.includes('='));
