import fs, { readFileSync } from 'fs';
import args from 'args';
import quitIfFileDoesNotExist from './services/fileExists';
import fileToLines from './services/fileToLines';
import checkForNecessaryFlags from './services/checkForNecessaryFlags';
import linesToJson from './services/linesToJson';
import relativePathToAbsolute from './services/relativePathToAbsolute';
import { diff } from 'json-diff';

export default (argv = process.argv):void => {
  args
    .options([
      { name: ['f', 'file'], description: 'File you want to parse' },
      { name: ['o', 'output'], description: 'Output to file' },
      { name: ['c', 'compare'], description: 'Compare changes of file to this file' },
    ]);

  const flags = args.parse(argv);
  checkForNecessaryFlags(flags);

  const filePath = relativePathToAbsolute(flags.file);
  quitIfFileDoesNotExist(filePath, true);

  const file = readFileSync(filePath, 'utf-8');
  const lines = fileToLines(file);
  let json = linesToJson(lines);

  if (flags.compare){
    const comparePath = relativePathToAbsolute(flags.compare);
    quitIfFileDoesNotExist(comparePath, true);
    const comparedFile = readFileSync(comparePath, 'utf-8');
    const comparedLines = fileToLines(comparedFile);
    const comparedJson = linesToJson(comparedLines);

    json = diff(json, comparedJson) || {};
  }

  if (flags.output){
    const outputPath = relativePathToAbsolute(flags.output);
    fs.writeFileSync(outputPath, JSON.stringify(json, null, 2));
  } else {
    process.stdout.write(JSON.stringify(json)+'\n');
  }
    
  
};